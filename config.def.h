/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 3;   /* border pixel of windows */
static const unsigned int snap           = 32;  /* snap pixel */
static const unsigned int gappih         = 10;  /* horiz inner gap between windows */
static const unsigned int gappiv         = 10;  /* vert inner gap between windows */
static const unsigned int gappoh         = 15;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 15;  /* vert outer gap between windows and screen edge */
static const int smartgaps_fact          = 1;   /* gap factor when there is only one client; 0 = no gaps, 3 = 3x outer gaps */
static const char autostartblocksh[]     = "autostart_blocking.sh";
static const char autostartsh[]          = "autostart.sh";
static const char dwmdir[]               = "dwm";
static const char localshare[]           = ".local/share";
static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
/* Status is to be shown on: -1 (all monitors), 0 (a specific monitor by index), 'A' (active monitor) */
static const int statusmon               = -1;
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int showsystray             = 1;   /* 0 means no systray */



/* Indicators: see patch/bar_indicators.h for options */
static int tagindicatortype              = INDICATOR_BOTTOM_BAR_SLIM;
static int tiledindicatortype            = INDICATOR_NONE;
static int floatindicatortype            = INDICATOR_TOP_LEFT_SQUARE;
static const char *fonts[]               = { "FiraCode Nerd Font Mono:style=Regular:size=14" };
static const char dmenufont[]            = "FiraCode Nerd Font:size=14";

static char c000000[]                    = "#000000"; // placeholder value

static char normfgcolor[]                = "#CDD6F4";
static char normbgcolor[]                = "#1E1E2E";
static char normbordercolor[]            = "#CBA6F7";
static char normfloatcolor[]             = "#F5C2E7";

static char selfgcolor[]                 = "#eeeeee";
static char selbgcolor[]                 = "#CBA6F7";
static char selbordercolor[]             = "#F9E2AF";
static char selfloatcolor[]              = "#F9E2AF";

static char titlenormfgcolor[]           = "#CDD6F4";
static char titlenormbgcolor[]           = "#1E1E2E";
static char titlenormbordercolor[]       = "#444444";
static char titlenormfloatcolor[]        = "#F5C2E7";

static char titleselfgcolor[]            = "#CBA6F7";
static char titleselbgcolor[]            = "#45475A";
static char titleselbordercolor[]        = "#45475A";
static char titleselfloatcolor[]         = "#45475A";

static char tagsnormfgcolor[]            = "#bbbbbb";
static char tagsnormbgcolor[]            = "#1E1E2E";
static char tagsnormbordercolor[]        = "#444444";
static char tagsnormfloatcolor[]         = "#F5C2E7";

static char tagsselfgcolor[]             = "#45475A";
static char tagsselbgcolor[]             = "#CBA6F7";
static char tagsselbordercolor[]         = "#CBA6F7";
static char tagsselfloatcolor[]          = "#CBA6F7";

static char hidnormfgcolor[]             = "#CBA6F7";
static char hidselfgcolor[]              = "#227799";
static char hidnormbgcolor[]             = "#1E1E2E";
static char hidselbgcolor[]              = "#1E1E2E";

static char urgfgcolor[]                 = "#CDD6F4";
static char urgbgcolor[]                 = "#1E1E2E";
static char urgbordercolor[]             = "#F38BA8";
static char urgfloatcolor[]              = "#F5C2E7";




static char *colors[][ColCount] = {
	/*                       fg                bg                border                float */
	[SchemeNorm]         = { normfgcolor,      normbgcolor,      normbordercolor,      normfloatcolor },
	[SchemeSel]          = { selfgcolor,       selbgcolor,       selbordercolor,       selfloatcolor },
	[SchemeTitleNorm]    = { titlenormfgcolor, titlenormbgcolor, titlenormbordercolor, titlenormfloatcolor },
	[SchemeTitleSel]     = { titleselfgcolor,  titleselbgcolor,  titleselbordercolor,  titleselfloatcolor },
	[SchemeTagsNorm]     = { tagsnormfgcolor,  tagsnormbgcolor,  tagsnormbordercolor,  tagsnormfloatcolor },
	[SchemeTagsSel]      = { tagsselfgcolor,   tagsselbgcolor,   tagsselbordercolor,   tagsselfloatcolor },
	[SchemeHidNorm]      = { hidnormfgcolor,   hidnormbgcolor,   c000000,              c000000 },
	[SchemeHidSel]       = { hidselfgcolor,    hidselbgcolor,    c000000,              c000000 },
	[SchemeUrg]          = { urgfgcolor,       urgbgcolor,       urgbordercolor,       urgfloatcolor },
};

/* Tags
 * In a traditional dwm the number of tags in use can be changed simply by changing the number
 * of strings in the tags array. This build does things a bit different which has some added
 * benefits. If you need to change the number of tags here then change the NUMTAGS macro in dwm.c.
 *
 * Examples:
 *
 *  1) static char *tagicons[][NUMTAGS*2] = {
 *         [DEFAULT_TAGS] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I" },
 *     }
 *
 *  2) static char *tagicons[][1] = {
 *         [DEFAULT_TAGS] = { "•" },
 *     }
 *
 * The first example would result in the tags on the first monitor to be 1 through 9, while the
 * tags for the second monitor would be named A through I. A third monitor would start again at
 * 1 through 9 while the tags on a fourth monitor would also be named A through I. Note the tags
 * count of NUMTAGS*2 in the array initialiser which defines how many tag text / icon exists in
 * the array. This can be changed to *3 to add separate icons for a third monitor.
 *
 * For the second example each tag would be represented as a bullet point. Both cases work the
 * same from a technical standpoint - the icon index is derived from the tag index and the monitor
 * index. If the icon index is is greater than the number of tag icons then it will wrap around
 * until it an icon matches. Similarly if there are two tag icons then it would alternate between
 * them. This works seamlessly with alternative tags and alttagsdecoration patches.
 */
static char *tagicons[][NUMTAGS*2] =
{
	[DEFAULT_TAGS]        = { "", "", "", "", "", "", "", "", "",
                              "", "", "", "", "", "", "", "", "" },
	[ALTERNATIVE_TAGS]    = { "A", "B", "C", "D", "E", "F", "G", "H", "I" },
	[ALT_TAGS_DECORATION] = { "<1>", "<2>", "<3>", "<4>", "<5>", "<6>", "<7>", "<8>", "<9>" },
};


/* There are two options when it comes to per-client rules:
 *  - a typical struct table or
 *  - using the RULE macro
 *
 * A traditional struct table looks like this:
 *    // class      instance  title  wintype  tags mask  isfloating  monitor
 *    { "Gimp",     NULL,     NULL,  NULL,    1 << 4,    0,          -1 },
 *    { "Firefox",  NULL,     NULL,  NULL,    1 << 7,    0,          -1 },
 *
 * The RULE macro has the default values set for each field allowing you to only
 * specify the values that are relevant for your rule, e.g.
 *
 *    RULE(.class = "Gimp", .tags = 1 << 4)
 *    RULE(.class = "Firefox", .tags = 1 << 7)
 *
 * Refer to the Rule struct definition for the list of available fields depending on
 * the patches you enable.
 */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *	WM_WINDOW_ROLE(STRING) = role
	 *	_NET_WM_WINDOW_TYPE(ATOM) = wintype
	 */
    /* All Monitors */
	RULE(.wintype = WTYPE "DIALOG",                                           .isfloating = 1                )
	RULE(.wintype = WTYPE "UTILITY",                                          .isfloating = 1                )
	RULE(.wintype = WTYPE "TOOLBAR",                                          .isfloating = 1                )
	RULE(.wintype = WTYPE "SPLASH",                                           .isfloating = 1                )

	RULE(.class = "Galculator",                                               .isfloating = 1                )
	RULE(.class = "kitty",                                    .tags = 1 << 1, .isfloating = 0, .monitor = -1 )
	RULE(.class = "org.wezfurlong.wezterm",                   .tags = 1 << 1, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Firefox",                                  .tags = 1 << 2, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Firefox",      .instance = "Toolkit",      .tags = 0,      .isfloating = 1, .monitor = -1 )
	RULE(.class = "Thunderbird",  .title = "Message Filters", .tags = 0,      .isfloating = 1, .monitor = -1 )
	RULE(.class = "Vivaldi-stable",                           .tags = 1 << 2, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Zathura",                                  .tags = 1 << 3, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Nemo",                                     .tags = 1 << 7, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Thunar",                                   .tags = 1 << 7, .isfloating = 0, .monitor = -1 )
	RULE(.class = "vifm",                                     .tags = 1 << 7, .isfloating = 0, .monitor = -1 )
	RULE(.class = "helvum",                                   .tags = 1 << 8, .isfloating = 1, .monitor = -1 )
	RULE(.class = "Lxappearance",                             .tags = 1 << 8, .isfloating = 0, .monitor = -1 )
	RULE(.class = "Blueman-manager",                          .tags = 1 << 8, .isfloating = 0, .monitor = -1 )
	RULE(.class = "openrgb",                                  .tags = 1 << 8, .isfloating = 0, .monitor = -1 )

    /* Primary Monitor */
	RULE(.class = "jetbrains-idea-ce",                        .tags = 1 << 0, .isfloating = 0, .monitor =  0 )
	RULE(.class = "obsidian",                                 .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "libreoffice-writer",                       .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "libreoffice-math",                         .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "libreoffice-impress",                      .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "libreoffice-calc",                         .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "libreoffice-draw",                         .tags = 1 << 3, .isfloating = 0, .monitor =  0 )
	RULE(.class = "Gimp",                                     .tags = 1 << 4, .isfloating = 1, .monitor =  0 )
	RULE(.class = "Blender",                                  .tags = 1 << 4, .isfloating = 1, .monitor =  0 )
	RULE(.class = "Shotwell",                                 .tags = 1 << 4, .isfloating = 0, .monitor =  0 )
	RULE(.class = "Nitrogen",                                 .tags = 1 << 4, .isfloating = 0, .monitor =  0 )
	RULE(.class = "Thunderbird",                              .tags = 1 << 5, .isfloating = 0, .monitor =  0 )
	RULE(.class = "tutanota-desktop",                         .tags = 1 << 5, .isfloating = 0, .monitor =  0 )
	RULE(.class = "Steam",                                    .tags = 1 << 6, .isfloating = 1, .monitor =  0 )
	RULE(.class = "heroic",                                   .tags = 1 << 6, .isfloating = 1, .monitor =  0 )
	RULE(.class = "Virt-manager",                             .tags = 1 << 8, .isfloating = 0, .monitor =  0 )

    /* Seconary Monitor */
	RULE(.class = "Chromium",                                 .tags = 1 << 2, .isfloating = 0, .monitor =  1 )
	RULE(.class = "discord",                                  .tags = 1 << 4, .isfloating = 0, .monitor =  1 )
	RULE(.class = "sengi",                                    .tags = 1 << 4, .isfloating = 0, .monitor =  1 )
	RULE(.class = "revolt-desktop",                           .tags = 1 << 4, .isfloating = 0, .monitor =  1 )
	RULE(.class = "Spotify",                                  .tags = 1 << 5, .isfloating = 0, .monitor =  1 )
	RULE(.class = "Cider",                                    .tags = 1 << 5, .isfloating = 0, .monitor =  1 )
	RULE(.class = "Ario",                                     .tags = 1 << 5, .isfloating = 0, .monitor =  1 )
	RULE(.class = "pocket-casts",                             .tags = 1 << 5, .isfloating = 0, .monitor =  1 )
	RULE(.class = "Bitwarden",                                .tags = 1 << 8, .isfloating = 0, .monitor =  1 )
};



/* Bar rules allow you to configure what is shown where on the bar, as well as
 * introducing your own bar modules.
 *
 *    monitor:
 *      -1  show on all monitors
 *       0  show on monitor 0
 *      'A' show on active monitor (i.e. focused / selected) (or just -1 for active?)
 *    bar - bar index, 0 is default, 1 is extrabar
 *    alignment - how the module is aligned compared to other modules
 *    widthfunc, drawfunc, clickfunc - providing bar module width, draw and click functions
 *    name - does nothing, intended for visual clue and for logging / debugging
 */
static const BarRule barrules[] = {
	/* monitor   bar    alignment         widthfunc                 drawfunc                clickfunc                hoverfunc                name */
	{ -1,        0,     BAR_ALIGN_LEFT,   width_tags,               draw_tags,              click_tags,              hover_tags,              "tags" },
	{  0,        0,     BAR_ALIGN_RIGHT,  width_systray,            draw_systray,           click_systray,           NULL,                    "systray" },
	{ -1,        0,     BAR_ALIGN_LEFT,   width_ltsymbol,           draw_ltsymbol,          click_ltsymbol,          NULL,                    "layout" },
	{ statusmon, 0,     BAR_ALIGN_RIGHT,  width_status2d,           draw_status2d,          click_status2d,          NULL,                    "status2d" },
	{ -1,        0,     BAR_ALIGN_NONE,   width_wintitle,           draw_wintitle,          click_wintitle,          NULL,                    "wintitle" },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */



static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
};


/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },



/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *roficmd[]  = { "rofi", "-show", "drun", NULL };
static const char *termcmd[]  = { "kitty", NULL };
static const char *webcmd[]   = { "firefox", NULL };
static const char *filecmd[]  = { "thunar", NULL };


static const Key keys[] = {
	/* modifier                     key            function                argument */
	{ MODKEY,                       XK_space,      spawn,                  {.v = roficmd } },
	{ MODKEY,                       XK_Return,     spawn,                  {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_Return,     spawn,                  {.v = webcmd } },
	{ MODKEY|ControlMask,           XK_Return,     spawn,                  {.v = filecmd } },
	{ MODKEY,                       XK_b,          togglebar,              {0} },
	{ MODKEY,                       XK_j,          focusstack,             {.i = +1 } },
	{ MODKEY,                       XK_k,          focusstack,             {.i = -1 } },
	{ MODKEY|ControlMask,           XK_j,          rotatestack,            {.i = +1 } },
	{ MODKEY|ControlMask,           XK_k,          rotatestack,            {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,          pushdown,               {0} },
	{ MODKEY|ShiftMask,             XK_k,          pushup,                 {0} },
	{ MODKEY,                       XK_m,          incnmaster,             {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_m,          incnmaster,             {.i = -1 } },
	{ MODKEY,                       XK_h,          setmfact,               {.f = -0.05} },
	{ MODKEY,                       XK_l,          setmfact,               {.f = +0.05} },
	{ MODKEY,                       XK_c,          zoom,                   {0} },
	{ MODKEY|ShiftMask,             XK_c,          view,                   {0} },
	{ MODKEY,                       XK_q,          killclient,             {0} },
	{ MODKEY|ShiftMask,             XK_q,          quit,                   {0} },
	{ MODKEY,                       XK_f,          togglefloating,         {0} },
	{ MODKEY,                       XK_0,          view,                   {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,          tag,                    {.ui = ~0 } },
	{ MODKEY,                       XK_comma,      focusmon,               {.i = -1 } },
	{ MODKEY,                       XK_period,     focusmon,               {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,      tagmon,                 {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,     tagmon,                 {.i = +1 } },
	{ MODKEY,                       XK_Tab,        cyclelayout,            {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Tab,        cyclelayout,            {.i = +1 } },
	TAGKEYS(                        XK_1,                                  0)
	TAGKEYS(                        XK_2,                                  1)
	TAGKEYS(                        XK_3,                                  2)
	TAGKEYS(                        XK_4,                                  3)
	TAGKEYS(                        XK_5,                                  4)
	TAGKEYS(                        XK_6,                                  5)
	TAGKEYS(                        XK_7,                                  6)
	TAGKEYS(                        XK_8,                                  7)
	TAGKEYS(                        XK_9,                                  8)
};


/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask           button          function        argument */
	{ ClkLtSymbol,          0,                   Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,                   Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,                   Button2,        zoom,           {0} },
	{ ClkStatusText,        0,                   Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,              Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,              Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,              Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,                   Button1,        view,           {0} },
	{ ClkTagBar,            0,                   Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,              Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,              Button3,        toggletag,      {0} },
};

