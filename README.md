# DWM - Patched

My patched version of dwm for BlackCat Linux

## Patches

- [alpha](https://dwm.suckless.org/patches/alpha/)
- [autostart](https://dwm.suckless.org/patches/autostart/)
- [statusallmons](https://dwm.suckless.org/patches/statusallmons/)
- [steam](https://dwm.suckless.org/patches/steam/)
- [ewmhtags](https://dwm.suckless.org/patches/ewmhtags/)
- [status2d](https://dwm.suckless.org/patches/status2d/)
- [systray](https://dwm.suckless.org/patches/systray/)
- [cyclelayouts](https://dwm.suckless.org/patches/cyclelayouts/)
- [centeredmaster](https://dwm.suckless.org/patches/centeredmaster/)
- [push](https://dwm.suckless.org/patches/push/)
- [rotatestack](https://dwm.suckless.org/patches/rotatestack/)
- [swallow](https://dwm.suckless.org/patches/swallow/)
- [vanitygaps](https://dwm.suckless.org/patches/vanitygaps/)

[original readme](https://gitlab.com/blackcat-linux/dwm/-/blob/main/README)
