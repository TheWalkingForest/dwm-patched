/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"
#include "bar.h"

#include "bar_ewmhtags.h"
#include "bar_ltsymbol.h"
#include "bar_status.h"
#include "bar_status2d.h"
#include "bar_tags.h"
#include "bar_wintitle.h"
#include "bar_systray.h"

/* Other patches */
#include "autostart.h"
#include "cyclelayouts.h"
#include "push.h"
#include "rotatestack.h"
#include "vanitygaps.h"
/* Layouts */
#include "layout_centeredmaster.h"
#include "layout_centeredfloatingmaster.h"
#include "layout_monocle.h"
#include "layout_tile.h"

